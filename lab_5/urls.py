from django.urls import path
from .views import get_note, index

app_name = 'lab_5'

urlpatterns = [
    path('', index, name = 'index'),
    path('/notes/<id>', get_note, name = 'notes'),
]