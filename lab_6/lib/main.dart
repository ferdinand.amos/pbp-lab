import 'package:flutter/material.dart';
import '../widgets/rating.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Rating Control",
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],

        // Define the default font family.
        fontFamily: 'Georgia',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
        home: const RatePage(title: "STAR RATE"),

    );
  }
}

class RatingsPage extends StatefulWidget {
  const RatingsPage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _RatingsPage createState() => _RatingsPage();
}

class _RatingsPage extends State<RatingsPage> {
  final _formKey = GlobalKey<FormState>();
  int _rating = 0;


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(title: Text("KLADAF RATE")),
        body: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Text(
                "Bagaimana menurut anda\ntentang web kami secara\nkeseluruhan?",
                style: TextStyle(fontSize: 24,),
                textAlign: TextAlign.center,
              ),

              SizedBox(height: size.height * 0.03),

              Rating((rating) {
                setState(() {
                  _rating = rating;
                });
              }, 5),

              SizedBox(height: size.height * 0.03),

          Container(
            width: 250.0,
            child: TextFormField(
                style: TextStyle(
                  fontSize: 15.0,
                  height: 2.0,
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  hintText: 'Describe your experience',
                ),
                validator: (value){
                  if(value!.isEmpty) {
                    return "type '-' if you dont have any comment";
                  }
                  return null;
                },
              ),
          ),

              SizedBox(height: size.height * 0.03),

              RaisedButton(
                child: const Text(
                  "POST",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    Navigator.pop(context);
                  }
                },
              ),
            ],
          ),
        ));
  }
}

class RatePage extends StatefulWidget {
  const RatePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _RatePage createState() => _RatePage();
}

class _RatePage extends State<RatePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Rating Card"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const RatingsPage(title: "RATE")),
            );
          },
          child: const Text('Rate'),
        ),
      ),
    );
  }
}