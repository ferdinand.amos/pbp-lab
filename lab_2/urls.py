from django.urls import path
from .views import xml, json, index

urlpatterns = [
    path('', index, name = 'index'),
    path('xml/', xml),
    path('json/', json),
]
