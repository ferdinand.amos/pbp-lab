1. Apakah perbedaan antara JSON dan XML?
    Json singkatan dari JavaScript Object Notation, 
   merupakan format pertukaran data yang ringan yang
   bisa digunakan untuk merepresentasikan data hierarkis
   dan didasarkan pada sintaksis objek JavaScript.
    Sedangkan XML merupakan versi sederhana dari SGML yang
   digunakan untuk menyimpan dan mewakili data tersetruktur
   dalam format yang bisa dibaca oleh mesin dan bisa dibaca
   oleh manusia.
    Perbedaanya adalah Secara sederhana, XML hanyalah bahasa 
   markup yang digunakan untuk menambahkan info tambahan ke 
   teks biasa, sedangkan JSON adalah cara yang efisien untuk 
   merepresentasikan data terstruktur dalam format yang 
   dapat dibaca manusia. XML lebih berorientasi ke dokumen
   sedangkan Json berorientasi ke data. Jadi JSON hanyalah
   format data dan XML merupakan bahasa markup.
   
2. Apakah perbedaan antara HTML dan XML?
    HTML merupakan markup language yang bertujuan untuk
   menyajikan data. Sedangkan XML merupakan markup language
   yang bertujuan untuk transfer data atau informasi.
   Dalam penulisannya XML lebih sensitive terhadap error,
   seperti kesalahan huruf besar atau huruf kecil memiliki
   arti yang berbeda sedangkan HTML cenderung mengabaikan error
   yang kecil. Tag XML bisa dikembangkan kembali, sedangkan tag 
   HTML terbatas.
    Jadi intinya keduanya merupakan markup language yang
   tujuannya berbeda, yaitu XML mentransfer data sedangakan HTML
   menyajikan data. XML cenderung bahasa yang sensitive terhadap 
   error dibandingkan HTML yang mengabaikan masalah error yang kecil.

Reference:
https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html