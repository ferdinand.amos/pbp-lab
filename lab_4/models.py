from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=200)
    noteFrom = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    message = models.TextField(max_length=256)
