from lab_4.models import Note
from django import forms

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'noteFrom', 'title', 'message' ]

        input_attrs = {
            'type': 'text',
            'placeholder' : 'Input here'
        }

        to = forms.CharField(label='', required=True, max_length=30, 
        widget= forms.TextInput(attrs=input_attrs))

        noteFrom = forms.CharField(label='', required=True, max_length=30, 
        widget= forms.TextInput(attrs=input_attrs))

        title = forms.CharField(label='',required=True, max_length=30,
        widget=forms.TextInput(attrs=input_attrs))

        message = forms.CharField(label='',required=True, max_length=256,
        widget=forms.TextInput(attrs=input_attrs))