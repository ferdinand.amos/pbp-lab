from django.urls import path
from .views import add_note, index, note_list 

app_name = 'lab_4'

urlpatterns = [
    path('', index, name = 'index'),
    path('noteForm', add_note, name = 'add_note'),
    path('noteList', note_list, name = 'note_list'),
]