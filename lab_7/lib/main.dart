import 'package:flutter/material.dart';
import '../widgets/rating.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Rating Control",
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],

        // Define the default font family.
        fontFamily: 'Georgia',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: const RatePage(title: "STAR RATE"),

    );
  }
}

class RatePage extends StatefulWidget {
  const RatePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _RatePage createState() => _RatePage();
}

class _RatePage extends State<RatePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Rating Card"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const RatingsPage(title: "RATE")),
            );
          },
          child: const Text('Rate'),
        ),
      ),
    );
  }
}

class RatingsPage extends StatefulWidget {
  const RatingsPage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _RatingsPage createState() => _RatingsPage();
}

class _RatingsPage extends State<RatingsPage> {
  final _formKey = GlobalKey<FormState>();
  int _rating = 0;
  String desc = "";


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(title: Text("KLADAF RATE")),
        body: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Text(
                "Bagaimana menurut anda\ntentang web kami secara\nkeseluruhan?",
                style: TextStyle(fontSize: 24,),
                textAlign: TextAlign.center,
              ),

              SizedBox(height: size.height * 0.03),

              Rating((rating) {
                setState(() {
                  _rating = rating;
                });
              }, 5),

              SizedBox(height: size.height * 0.03),

              Container(
                width: 250.0,
                child: TextFormField(
                  style: TextStyle(
                    fontSize: 15.0,
                    height: 2.0,
                  ),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    hintText: 'Describe your experience',
                  ),
                  validator: (value){
                    if(value!.isEmpty) {
                      return "type '-' if you dont have any comment";
                    }
                    desc = value;
                    return null;
                  },
                ),
              ),

              SizedBox(height: size.height * 0.03),

              ElevatedButton(
                child: const Text(
                  "POST",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => PageTwo(
                        rating: _rating,
                        description: desc,
                        key: this._formKey,
                  ),),);
                  }
                },
                style: ElevatedButton.styleFrom(
                    primary: Colors.purple,
                    padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                    textStyle: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold)),
              ),
            ],
          ),
        ));
  }
}

class PageTwo extends StatefulWidget {
  final int rating;
  final String description;
  const PageTwo(
      {required Key key, required this.rating, required this.description})
      : super(key: key);
//super is used to call the constructor of the base class which is the StatefulWidget here
  @override
  _PageTwoState createState() => _PageTwoState();
}
class _PageTwoState extends State<PageTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 2'),
        automaticallyImplyLeading: false, //optional: removes the default back arrow
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Rate: ${widget.rating} was passed',
            style: TextStyle(fontSize: 24,),
            textAlign: TextAlign.center,
          ),

          Text(
            'Description: ${widget.description}',
            style: TextStyle(fontSize: 24,),
            textAlign: TextAlign.center,
          ),
//${} sign here prints the value of variable inside it.
          RaisedButton(
              color: Colors.grey,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('<- Go back'))
        ],
      ),
    );
  }
}
