from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm()

    if request.method == 'POST':
        form = FriendForm(request.POST)
        form.save()
        return HttpResponseRedirect('/lab-3/')

    context = {'form' : form}
    return render(request, "lab3_form.html", context)